package com.example.android.hilt.contentprovider

import android.content.*
import android.database.Cursor
import android.net.Uri
import com.example.android.hilt.data.LogDao
import dagger.hilt.EntryPoint
import dagger.hilt.InstallIn
import dagger.hilt.android.EntryPointAccessors
import dagger.hilt.components.SingletonComponent

const val LOGS_TABLE = "logs"

const val AUTHORITY = "com.example.android.hilt.provider"

const val CODE_LOGS_DIR = 1

const val CODE_LOGS_ITEM = 2

/**
 * A ContentProvider that exposes the logs outside the application process.
 */
class LogsContentProvider : ContentProvider() {

    private val uriMatcher = UriMatcher(UriMatcher.NO_MATCH).apply {
        addURI(AUTHORITY, LOGS_TABLE, CODE_LOGS_DIR)
        addURI(AUTHORITY, "$LOGS_TABLE/*", CODE_LOGS_ITEM)
    }

    override fun onCreate(): Boolean {
        return true
    }

    /**
     * Queries all the logs or an individual log from the log database.
     * */
    override fun query(
        uri: Uri,
        projection: Array<out String>?,
        selection: String?,
        selectionArgs: Array<out String>?,
        sortOrder: String?
    ): Cursor {
        val code = uriMatcher.match(uri)
        return if (code == CODE_LOGS_DIR || code == CODE_LOGS_ITEM) {
            val appContext = context?.applicationContext
                ?: throw IllegalStateException("Null context in operation.")
            val logDao = getLogDao(appContext)
            val cursor = if (code == CODE_LOGS_DIR) {
                logDao.selectAllLogs()
            } else {
                logDao.selectLogById(ContentUris.parseId(uri))
            }
            cursor.setNotificationUri(appContext.contentResolver, uri)
            cursor
        } else {
            throw IllegalArgumentException("Unknown URI:$uri")
        }
    }

    override fun getType(uri: Uri): String? {
        throw UnsupportedOperationException("Only reading operation are allowed.")
    }

    override fun insert(uri: Uri, values: ContentValues?): Uri? {
        throw UnsupportedOperationException("Only reading operation are allowed.")
    }

    override fun delete(uri: Uri, selection: String?, selectionArgs: Array<out String>?): Int {
        throw UnsupportedOperationException("Only reading operation are allowed.")
    }

    override fun update(
        uri: Uri,
        values: ContentValues?,
        selection: String?,
        selectionArgs: Array<out String>?
    ): Int {
        throw UnsupportedOperationException("Only reading operation are allowed.")
    }

    private fun getLogDao(appContext: Context): LogDao {
        val hiltEntryPoint = EntryPointAccessors.fromApplication(
            appContext,
            LogContentProviderEntryPoint::class.java
        )
        return hiltEntryPoint.logDao()
    }

    /**
     * Annotation for marking an interface as an entry point into a generated component.
     * This annotation must be used with InstallIn to indicate which component(s) should
     * have this entry point. When assembling components, Hilt will make the indicated
     * components extend the interface marked with this annotation.
     * */
    @EntryPoint
    @InstallIn(SingletonComponent::class)
    interface LogContentProviderEntryPoint {
        fun logDao(): LogDao
    }
}